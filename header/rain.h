#include <SFML/Graphics.hpp>
#include <vector>

#ifndef RAIN_H
#define RAIN_H


class Rain
{
    private:

        sf::Vector2u m_windowSize;

        int m_qtdRainDrop;
        std::vector<sf::RectangleShape> m_rainDrops;        

        sf::Color CreateColor();
        sf::RectangleShape CreateRainDrop();
        
    public:
        Rain(const int & l_qtdRainDrop, const sf::Vector2u l_windowSize);

        sf::RectangleShape& GetRainDrop(int l_index) { return this->m_rainDrops[l_index]; }
        int GetQtdRainDrop() const { return this->m_qtdRainDrop; };
        

        void UpdateRainDrop();

};


#endif