#!/bin/bash
g++ -c ./source/*.cpp
g++ *.o -o simulation -Llib/sfml/lib -lsfml-graphics -lsfml-window -lsfml-system
rm *.o
